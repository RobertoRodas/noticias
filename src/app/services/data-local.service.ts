import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Article } from '../pages/interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

  noticias: Article [] = [];

  constructor(private storage: Storage) { 

    this.cargarFavoritos();

  }


  guardarNoticia(noticia: Article) {
    
    const existe = this.noticias.find( noti => noti.title === noticia.title);
    
    if (!existe){
      this.noticias.unshift(noticia);
      this.storage.set('Favoritos', this.noticias);
      return true;
    }
    return false;
  }

  borrarNoticia(noticia: Article) {
    this.noticias = this.noticias.filter( noti => noti.title !== noticia.title );

    this.storage.set('Favoritos', this.noticias);

  }

  async cargarFavoritos() {
    // this.storage.get('Favoritos')
    // .then( favorito => {
    //   console.log('Favoritos: ', favorito);
    // });
    const favoritos = await this.storage.get('Favoritos');

    console.log('async await: ', favoritos);

    if (favoritos) {
      this.noticias = favoritos;
    }
  }

}
