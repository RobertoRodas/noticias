import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RespuestaToHeadlines } from '../pages/interfaces/interfaces';
import { environment } from '../../environments/environment.prod';


const apiKey = environment.apiKey;
const apiUrl = environment.apiUrl;

const headers = new HttpHeaders({
  'X-Api-Key': apiKey
});


@Injectable({
  providedIn: 'root'
})
export class NoticiasService {

  constructor(private http: HttpClient ) { }

  headlinesPages = 0;

  categoriaActual = '';
  categoriaPage = 0;

  private ejecutarQuery<T>( query: string ) {
    console.log('apiKey: ', apiKey );
    console.log('apiUrl: ', apiUrl );
    query = apiUrl + query;
    // console.log('query: ', query );
    return this.http.get<T>(query, { headers });
  }

  getTopHeadlines() {
    this.headlinesPages++;
    console.log('/top-headlines?country=mx&page=' + this.headlinesPages);
    // return this.http.get<RespuestaToHeadlines>('http://newsapi.org/v2/top-headlines?country=mx&category=business&apiKey=7d4a61fba9504f178233eccb36d50ad9');
    // return this.http.get<RespuestaToHeadlines>('https://newsapi.org/v2/top-headlines?country=mx&apiKey=7d4a61fba9504f178233eccb36d50ad9&page=1');
    return this.ejecutarQuery<RespuestaToHeadlines>('/top-headlines?country=mx&page=' + this.headlinesPages);
  }

  getTopHeadlinesCategoria( categoria: string ) {

    if (this.categoriaActual === categoria) {
      this.categoriaPage++;
    } else {
      this.categoriaPage = 1;
      this.categoriaActual = categoria;
    }

    // return this.http.get('https://newsapi.org/v2/top-headlines?country=de&category=business&apiKey=7d4a61fba9504f178233eccb36d50ad9');
    return this.ejecutarQuery<RespuestaToHeadlines>('/top-headlines?country=mx&category=' + categoria + '&page=' + this.categoriaPage);
  }

}
