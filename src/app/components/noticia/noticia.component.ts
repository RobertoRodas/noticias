import { Component, OnInit, Input } from '@angular/core';
import { Article } from '../../pages/interfaces/interfaces';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { ActionSheetController, ToastController } from '@ionic/angular';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { DataLocalService } from '../../services/data-local.service';

@Component({
  selector: 'app-noticia',
  templateUrl: './noticia.component.html',
  styleUrls: ['./noticia.component.scss'],
})
export class NoticiaComponent implements OnInit {

  @Input() noticia: Article;
  @Input() i: number;
  @Input() enFavorito;

  constructor(
    private iab: InAppBrowser,
    private actionSheetCtrl: ActionSheetController,
    private socialSharing: SocialSharing,
    private dataLocalService: DataLocalService,
    public toastCtrl: ToastController
    ) { }

  ngOnInit() {
    console.log('enFavorito', this.enFavorito);
  }

  abrirNoticia() {
    // console.log('Noticia: ', this.noticia.url);
    const browser = this.iab.create(this.noticia.url, '_system');
    // const browser = this.iab.create(this.noticia.url);
    // const browser = this.iab.create('https://ionicframework.com/docs/native/in-app-browser#installation', '_system');
  }

  async mostrarMensaje(mensaje: string) {
    const toast = await this.toastCtrl.create({
      message: mensaje,
      duration: 2000
    });
    toast.present();
  }

  async lanzarMenu() {
    let guardarBorrarBtn;

    if (this.enFavorito) {
      guardarBorrarBtn = {
        text: 'Borrar Favorito',
        icon: 'trash',
        cssClass: 'action-dark',
        handler: () => {
          console.log('Borrar Favorito');
          this.dataLocalService.borrarNoticia(this.noticia);
          this.mostrarMensaje('¡Se ha borrar de Favoritos!');
        }
      };
    } else {
      guardarBorrarBtn = {
        text: 'Favorito',
        icon: 'star',
        cssClass: 'action-dark',
        handler: () => {
          console.log('Favorito');
          let accion = this.dataLocalService.guardarNoticia(this.noticia);
          if (accion) {
            this.mostrarMensaje('Añadido a Favoritos');
          } else {
            this.mostrarMensaje('Ya esta Añadido');
          }
        }
      };
    }   


    const actionSheet = await this.actionSheetCtrl.create({
      cssClass: 'my-custom-class',
      buttons: [
        {
          text: 'Compartir',
          icon: 'share',
          cssClass: 'action-dark',
          handler: () => {
            console.log('Share clicked');
            this.socialSharing.share(
              this.noticia.title,
              this.noticia.source.name,
              '',
              this.noticia.url
            );
          }
        }, 
        guardarBorrarBtn,
        {
          text: 'Cancelar',
          icon: 'close',
          cssClass: 'action-dark',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
      }]
    });
    await actionSheet.present();
  }
}
