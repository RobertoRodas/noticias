import { Component, OnInit } from '@angular/core';
import { NoticiasService } from '../../services/noticias.service';
import { Article } from '../interfaces/interfaces';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  noticias: Article[] = [];

  constructor(private NoticiasService: NoticiasService) {}

  ngOnInit() {
    console.log('ngOnInit');
    this.cargarNoticia();
  }

  loadData(event) {
    console.log('loadData');
    console.log(event);

    this.cargarNoticia(event);
  }

  cargarNoticia(event?) {
    this.NoticiasService.getTopHeadlines()
      .subscribe( respuesta => {
        console.log('Noticias', respuesta);
        this.noticias.push( ...respuesta.articles );
        if (event) {
          event.target.complete();
        }
        console.log('Cantidad: ', respuesta.articles.length);
        if ( respuesta.articles.length == 0 ){
          console.log('dismiss');
          event.target.disabled = true;

        }
    });
  }

}
