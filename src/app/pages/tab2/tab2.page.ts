import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSegment } from '@ionic/angular';
import { NoticiasService } from '../../services/noticias.service';
import { Article } from '../interfaces/interfaces';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  @ViewChild(IonSegment) segment: IonSegment;
  // @ViewChild('segmento', {static: true}) segment: IonSegment;

  categorias = ['business', 'entertainment', 'general', 'health', 'science', 'sports', 'technology'];
  noticias: Article[] = [];

  constructor(private noticiasService: NoticiasService) {}

  ionViewDidEnter() {
    this.segment.value = this.categorias[0];
    this.cargarNoticias(this.categorias[0]);
  }

  cambioCategoria(event) {

    this.noticias = [];

    this.cargarNoticias(event.detail.value);
  }

  cargarNoticias( categorias: string, event?) {

    this.noticiasService.getTopHeadlinesCategoria(categorias)
      .subscribe( respuesta => {
        console.log(respuesta);
        this.noticias.push( ...respuesta.articles );

        if (event) {
          event.target.complete();
        }

        if ( respuesta.articles.length == 0 ){
          console.log('dismiss');
          event.target.disabled = true;

        }
    });
  }

  loadData(event) {
    this.cargarNoticias( this.segment.value, event );
  }
}
